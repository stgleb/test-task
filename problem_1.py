# == instead of = assignment.

print reduce(lambda x, y: x+y, filter(lambda x: x % 2,
                                      map(lambda x: x*x, xrange(10**6)))) == \
      sum(x*x for x in xrange(1, 10**6, 2))
