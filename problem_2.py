input = [0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2]
used = set()


# function that filters repeated elements.
def f(arg):
    global used

    if arg[1] not in used:
        used.add(arg[1])
        return True
    else:
        return False

# filtering 5-th and 7-th elements, and map function that removes duplicates.
print map(lambda x: x[1],
          filter(f, filter(lambda x: x[0] % 5 == 0 or x[0] % 7 == 0,
                           enumerate(input))))

# alternative using list -> set -> set transformation
# ugly code
print list(set([x[1] for x in
                filter(lambda x: x[0] % 5 == 0 or x[0] % 7 == 0,
                       enumerate(input))]))


# simple function that filters
def my_filter_function(sequence):
    sequence = [sequence[i] for i in range(len(sequence))
                if i % 5 == 0 or i % 7 == 0]

    cache = set()
    output = []

    for elem in sequence:
        if elem not in cache:
            output.append(elem)
            cache.add(elem)

    return output


print my_filter_function(input)
