

# function generates first numbers of fibonacci sequence.
# returns whole list of values
def fib(n):
    a, b = 0, 1
    result = []

    for i in range(n):
        a, b = a + b, a
        result.append(b)

    return result


# generator based function that returns generator of fibonacci sequence.
def fib_generator(n):
    a, b = 0, 1
    result = []

    for i in range(n):
        a, b = a + b, a
        yield b


# function computes n-th element of fibonacci sequence
def fib_recursive(n):
    if n < 0:
        return n
    return fib_recursive(n - 1) + fib_recursive(n - 2)


# memoized function that generates n-th fibonacci number
def fib_memoized(n, cache):
    if n < 2:
        return n

    if n in cache:
        return cache[n]
    else:
        result = fib_memoized(n - 1, cache) + fib_memoized(n - 2, cache)
        cache[n] = result
        return result


def fib_rec_gen(n):
    if n < 2:
        yield n
    else:
        fib_rec_gen(n - 1) + fib_rec_gen(n - 2)


# function generates fibonacci sequence
# in recursive manner with aid of generators.
def f(a, b):
    yield a

    for x in f(a + b, a):
        yield x


for x in f(1, 0):
    print x
