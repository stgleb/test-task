# merge sort implementation
def sort(lst, beg, end):
    if end - beg <= 1:
        return

    mid = (beg + end) / 2
    sort(lst, beg, mid)
    sort(lst, mid, end)

    buf = lst[beg:end]

    i = beg
    j = mid
    cnt = 0

    while i < mid and j < end:
        if lst[i] < lst[j]:
            buf[cnt] = lst[i]
            i += 1
        else:
            buf[cnt] = lst[j]
            j += 1

        cnt += 1

    while i < mid:
        buf[cnt] = lst[i]
        i += 1
        cnt += 1

    while j < end:
        buf[cnt] = lst[j]
        j += 1
        cnt += 1

    for i in range(len(buf)):
        lst[beg + i] = buf[i]


a = [1, 2, 0, 3, 8, 6, 5, 9]
sort(a, 0, len(a))
print a
